<?php

namespace Boomdraw\Socket\Tests;

use Boomdraw\Socket\Socket;
use PHPUnit\Framework\TestCase;
use Boomdraw\Socket\Exceptions\SocketException;

class SocketTest extends TestCase
{
    /**
     * @test
     */
    public function it_sends_requests_and_gets_responses()
    {
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true, 'connect_timeout' => 5]);
        $socket = $socket->create()->create()->connect()->send("GET / HTTP/1.1\r\n\r\n");
        $response = $socket->readAll();
        $socket->close();
        self::assertStringContainsString('HTTP/1.1 200 OK', $response);
    }

    /**
     * @test
     */
    public function it_reads_from_socket()
    {
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->create()->connect();
        $socket->write("GET / HTTP/1.1\r\n\r\n");
        $response = $socket->read();
        $socket->close();
        self::assertEquals('HT', $response);
    }

    public function socket_response_matches()
    {
        $get = [
            'array' => [
                'key' => 'value'
            ]
        ];
        $post = [
            'key1' => 1
        ];

        $request = $this->client->post('/?array[key]=value', [
            'json' => $post
        ]);

        self::assertEquals(200, $request->getStatusCode(), "Post with json return 200");
        $json = \GuzzleHttp\json_decode($request->getBody(), true);

        self::assertEquals("POST", $json['method'], "Requested method match");
        self::assertEquals("application/json", $json['Content-Type'], "Content-Type match");
        self::assertEquals($post, $json['post'], "Post data match");
        self::assertEquals($get, $json['get'], "Get data match");
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unable_to_create_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessage('Cannot create socket.');
        $socket = new Socket(__DIR__ . '/../../socat.sock', [], 99, 99, 99);
        $socket->create();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unable_to_connect_to_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessageMatches('/^Cannot connect socket to+/u');
        $socket = new Socket(__DIR__ . '/../socat.sock', ['debug' => true]);
        $socket->create()->create()->connect()->write("GET / HTTP/1.1\r\n\r\n")->close();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_writing_to_empty_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessage('Cannot write to empty socket.');
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->write("GET / HTTP/1.1\r\n\r\n")->close();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unable_to_send_to_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessageMatches('/^Error occur when write to stream.+/u');
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->create()->send('hello', 99)->close();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_sending_to_empty_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessage('Cannot send data to empty socket.');
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->send()->close();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unable_to_read_from_empty_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessage('Cannot read from empty socket.');
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->read();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unable_to_read_all_from_empty_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessage('Cannot read from empty socket.');
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->readAll();
    }

    /**
     * @test
     */
    public function it_throws_exception_when_unable_to_read_all_from_socket()
    {
        $this->expectException(SocketException::class);
        $this->expectExceptionMessageMatches('/Error occur when read from stream:+/u');
        $socket = new Socket(__DIR__ . '/../../socat.sock', ['debug' => true]);
        $socket->create()->connect();
        $socket->write("GET / HTTP/1.1\r\n\r\n");
        $socket->readAll(99, -1);
        $socket->close();
    }
}
