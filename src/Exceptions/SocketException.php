<?php

namespace Boomdraw\Socket\Exceptions;

use RuntimeException;

class SocketException extends RuntimeException
{
}
