<?php

namespace Boomdraw\Socket;

use Boomdraw\Socket\Exceptions\SocketException;

class Socket
{
    /** @var resource|null socket instance */
    protected $socket = null;

    /** @var string|null socket path */
    protected ?string $path = null;

    /** @var int socket_create $domain parameter */
    protected int $domain = AF_UNIX;

    /** @var int socket_create $type parameter */
    protected int $type = SOCK_STREAM;

    /** @var int socket_create $type parameter */
    protected int $protocol = SOL_SOCKET;

    /** @var bool debug flat */
    protected bool $debug = false;

    /** @var float|null */
    protected ?float $timeout = null;

    /**
     * Socket constructor.
     *
     * @param string $path
     * @param array $options associative array with keys from SocketOptions class
     * @param int $domain
     * @param int $type
     * @param int $protocol
     */
    public function __construct(string $path, array $options = [], int $domain = AF_UNIX, int $type = SOCK_STREAM, int $protocol = SOL_SOCKET)
    {
        $this->path = $path;
        $this->domain = $domain;
        $this->type = $type;
        $this->protocol = $protocol;

        $this->applyOptions($options);
    }

    /**
     * Socket destructor, call close method
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * Create new socket if not exist
     *
     * @return $this
     * @throws SocketException;
     */
    public function create(): self
    {
        if ($this->debug) {
            echo __METHOD__ . PHP_EOL;
        }

        // if socket is already created - do nothing
        if (isset($this->socket)) {
            return $this;
        }

        $this->socket = @socket_create($this->domain, $this->type, $this->protocol);
        if (!$this->socket) {
            $this->throwSocketException('Cannot create socket.');
        }

        if ($this->timeout > 0) {
            $this->setSocketTimeout($this->timeout);
        }

        return $this;
    }

    /**
     * Connect to socket
     *
     * @return $this
     * @throws SocketException
     */
    public function connect(): self
    {
        if ($this->debug) {
            echo __METHOD__ . PHP_EOL;
        }

        if (false === @socket_connect($this->socket, $this->path)) {
            $this->throwSocketException("Cannot connect socket to {$this->path}.");
        }

        return $this;
    }

    /**
     * Write to socket
     *
     * @param string $message
     * @return $this
     * @throws SocketException
     */
    public function write(string $message): self
    {
        if ($this->debug) {
            echo __METHOD__ . '[DEBUG]' . PHP_EOL . $message . PHP_EOL . '[/DEBUG]' . PHP_EOL;
        }

        if (!isset($this->socket)) {
            throw new SocketException('Cannot write to empty socket.');
        }

        if (false === @socket_write($this->socket, $message, strlen($message))) {
            $this->throwSocketException('Error occur when write to stream.');
        }

        return $this;
    }

    /**
     * Send $message to stream with flag set
     *
     * @param string $message
     * @param int $flag socket_send $flag parameter
     * @return $this
     * @throws SocketException
     */
    public function send(string $message = '', int $flag = MSG_EOR): self
    {
        if ($this->debug) {
            echo __METHOD__ . '[DEBUG]' . PHP_EOL . $message . PHP_EOL . '[/DEBUG]' . PHP_EOL;
        }

        if (!isset($this->socket)) {
            throw new SocketException('Cannot send data to empty socket.');
        }

        if (false === @socket_send($this->socket, $message, strlen($message), $flag)) {
            $this->throwSocketException('Error occur when write to stream.');
        }

        return $this;
    }

    /**
     * Read from socket.
     *
     * @param int $type socket_read $type parameter
     * @param int $length socket_read $length parameter
     * @return string
     * @throws SocketException
     */
    public function read(int $type = PHP_BINARY_READ, int $length = 65384): string
    {
        if ($this->debug) {
            echo __METHOD__ . PHP_EOL;
        }
        if (!isset($this->socket)) {
            throw new SocketException('Cannot read from empty socket.');
        }

        return $this->readChunk($length, $type);
    }

    /**
     * Read all from socket.
     *
     * @param int $type socket_read $type parameter
     * @param int $chunkLength socket_read $length parameter
     * @return string
     * @throws SocketException
     */
    public function readAll(int $type = PHP_BINARY_READ, int $chunkLength = 65384): string
    {
        if ($this->debug) {
            echo __METHOD__ . PHP_EOL;
        }
        if (!isset($this->socket)) {
            throw new SocketException('Cannot read from empty socket.');
        }

        $response = '';
        while ($partial = $this->readChunk($type, $chunkLength)) {
            $response .= $partial;
        }

        return $response;
    }

    /**
     * Close socket.
     *
     * @return $this
     */
    public function close(): self
    {
        if ($this->debug) {
            echo __METHOD__ . PHP_EOL;
        }

        if (isset($this->socket) && !is_bool($this->socket)) {
            socket_close($this->socket);
            $this->socket = null;
        }

        return $this;
    }

    /**
     * Set socket timeout.
     *
     * @param float $timeout
     */
    protected function setSocketTimeout(float $timeout): void
    {
        $success = true;
        $s = (int)$this->timeout;
        $ms = (int)(($this->timeout - $s) * 1000);
        $success &= socket_set_option($this->socket, $this->protocol, SO_RCVTIMEO, ['sec' => $s, 'usec' => $ms]);
        $success &= socket_set_option($this->socket, $this->protocol, SO_SNDTIMEO, ['sec' => $s, 'usec' => $ms]);

        if (!$success) {
            trigger_error('Cannot set socket timeout', E_USER_WARNING);
        }
    }

    /**
     * Read and return socket chunk.
     *
     * @param int $type socket_read $type parameter
     * @param int $length socket_read $length parameter
     * @return string
     * @throws SocketException
     */
    protected function readChunk(int $type, int $length): string
    {
        $partial = @socket_read($this->socket, $length, $type);
        if (false === $partial) {
            $this->throwSocketException('Error occur when read from stream: ');
        }

        return $partial;
    }

    /**
     * Apply specified socket options.
     *
     * @param array $options
     * @return $this
     */
    protected function applyOptions(array $options): self
    {
        if (isset($options[SocketOptions::SOCKET_TIMEOUT])) {
            $this->timeout = (float)$options[SocketOptions::SOCKET_TIMEOUT];
        }
        if (isset($options[SocketOptions::SOCKET_DEBUG])) {
            $this->debug = (bool)$options[SocketOptions::SOCKET_DEBUG];
        }

        return $this;
    }

    /**
     * Extract socket errors and throw SocketException
     *
     * @param string $message
     * @return void
     * @throws SocketException
     */
    protected function throwSocketException(string $message): void
    {
        if (is_bool($this->socket)) {
            throw new SocketException($message);
        }

        $lastError = socket_last_error($this->socket);
        $errorMessage = socket_strerror($lastError);
        socket_clear_error($this->socket);

        throw new SocketException("$message $errorMessage", $lastError);
    }
}
