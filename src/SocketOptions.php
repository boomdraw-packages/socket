<?php

namespace Boomdraw\Socket;

class SocketOptions
{
    /**
     * Socket connection/read/write timeouts (float)
     */
    public const SOCKET_TIMEOUT = 'connect_timeout';

    /**
     * socket debug flag (bool)
     */
    public const SOCKET_DEBUG = 'debug';
}
